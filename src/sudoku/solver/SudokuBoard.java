package sudoku.solver;

/**
 *
 * @author mjafar
 */
public final class SudokuBoard {

    /**
     * Board cells matrix
     */
    private SudokuBoardCell[][] blocks;

    /**
     * Constructor
     */
    public SudokuBoard() {
        blocks = new SudokuBoardCell[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                blocks[i][j] = new SudokuBoardCell();
            }
        }
    }

    /**
     * Get cell in desired row and column.
     *
     * @param row row
     * @param col column
     * @return SudokuBoardCell
     * @throws IllegalArgumentException when block <i>row</i> or <i>col</i> are
     * out of range [1-9]
     */
    public SudokuBoardCell get(int row, int col) throws IllegalArgumentException {
        if (row < 1 || col < 1 || row > 9 || col > 9) {
            throw new IllegalArgumentException("Block out of range.");
        }

        return blocks[--row][--col];
    }

    /**
     * Checks if <i>value</i> is repetitive in 3x3 blocks or not.
     *
     * @param row
     * @param col
     * @param value
     * @return
     */
    public boolean repetetiveInBlock(int row, int col, int value) {
        get(row, col); // For bounds check
        
        row--;
        col--;
        int blockRow = row / 3;
        int blockCol = col / 3;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (blocks[blockRow * 3 + i][blockCol * 3 + j].getValue() == value) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks if <i>value</i> is repetitive in a row
     *
     * @param row
     * @param value
     * @return true if row contains <i>value</i>
     */
    public boolean repetetiveInRow(int row, int value) {
        for (int i = 1; i <= 9; i++) {
            if (get(row, i).getValue() == value) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if <i>value</i> is repetitive in a column
     *
     * @param col
     * @param value
     * @return true if column contains <i>value</i>
     */
    public boolean repetetiveInColumn(int col, int value) {
        for (int i = 1; i <= 9; i++) {
            if (get(i, col).getValue() == value) {
                return true;
            }
        }

        return false;
    }

    public boolean hasOnlyOnePossibeNumberAnswers() {
        for (int row = 1; row <= 9; row++) {
            for (int col = 1; col <= 9; col++) {
                if (get(row, col).hasPossibleValues() && get(row, col).possibleValuesCount() == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setOnlyOnePossibleNumberAswers() {
        int possibleAnswer;
        for (int row = 1; row <= 9; row++) {
            for (int col = 1; col <= 9; col++) {
                if (get(row, col).hasPossibleValues() && get(row, col).possibleValuesCount() == 1) {
                    possibleAnswer = get(row, col).getPossibleValues().iterator().next();
                    get(row, col).setValue(possibleAnswer);
                    get(row, col).getPossibleValues().clear();
                    removeValueFromPossibilitiesInRowColBox(row, col, possibleAnswer);
                }
            }
        }
    }

    public int getNumberOfNonZeros() {
        int numberOfNonZeros = 0;
        for (int row = 1; row <= 9; row++) {
            for (int col = 1; col <= 9; col++) {
                if (get(row, col).isSet()) {
                    numberOfNonZeros++;
                }
            }
        }
        return numberOfNonZeros;
    }

    public boolean hasZeros() {
        for (int row = 1; row <= 9; row++) {
            for (int col = 1; col <= 9; col++) {
                if (!get(row, col).isSet() && !get(row, col).hasPossibleValues()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void removeValueFromPossibilitiesInRowColBox(int row, int col, int possibleAnswer) {
        // Row and column
        for (int i = 1; i <= 9; i++) {
            if (i != row) {
                get(i, col).removePossibleValue(possibleAnswer);
            }
            if (i != col) {
                get(row, i).removePossibleValue(possibleAnswer);
            }
        }

        // Box
        row--;
        col--;
        int blockRow = row / 3;
        int blockCol = col / 3;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (blockRow * 3 + i != row && blockCol * 3 + j != col) {
                    blocks[blockRow * 3 + i][blockCol * 3 + j].removePossibleValue(possibleAnswer);
                }
            }
        }
    }

    /**
     * Resets the table
     */
    public void clear() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                blocks[i][j].getPossibleValues().clear();
                blocks[i][j].setValue(0);
            }
        }
    }
}
