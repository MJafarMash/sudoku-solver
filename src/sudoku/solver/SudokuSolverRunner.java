package sudoku.solver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UnsupportedLookAndFeelException;
import sudoku.solver.CLI.CLISet;
import sudoku.solver.swing.SudokuSolverGUI;

/**
 *
 * @author mjafar
 */
public class SudokuSolverRunner {

    private static boolean graphicalInterface;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        graphicalInterface = true;
        final SudokuSolver solver;
        String file = null;


        if (args.length > 0) {
            if (args[0].charAt(0) == '-') {
                switch (args[0].toLowerCase()) {
                    case "-g":
                    case "--graphic":
                    case "--graphical":
                    case "--gui":
                        graphicalInterface = true;
                        break;
                    case "-c":
                    case "--cli":
                    case "--text":
                    case "--console":
                        graphicalInterface = false;
                        break;
                    case "/?":
                    case "-h":
                    case "?":
                    case "--help":
                        System.out.println("Sudoku solver");
                        System.out.println("USAGE:");
                        System.out.println("SudokuSolver.jar [OPTIONS] [LOAD FILE ADDRESS]");
                        System.out.println("OPTIONS");
                        System.out.println("\t-g --gui --graphic --graphical : Opens swing gui interface.");
                        System.out.println("\t-c --cli --text --console: Opens CLI interface.");
                        System.out.println();
                        return;
                }
                if (args.length == 2) {
                    file = args[1];
                }
            } else {
                file = args[0];
            }
        }


        System.out.printf("Running in %s mode.\n", graphicalInterface ? "GUI" : "CLI");
        //
        solver = new SudokuSolver();
        //
        if (file != null) {
            try {
                readFromFile(file, solver);
            } catch (IOException e) {
                System.err.println("Error reading from " + file + "\n" + e.getMessage());
            }
        }
        if (graphicalInterface) {
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
                    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    }
                    new SudokuSolverGUI(solver).setVisible(true);
                }
            });
        } else {
            SudokuSolverCLI.getData(solver);
        }
    }

    /**
     * Load initial values from a file.
     *
     * File content should be in this format:<br>
     * {Row Number} {Column Number} {Value}<br>
     * Row and Column numbers start from 1
     *
     * @param file file address to read from.
     * @param solver instance of {@link SudokuSolver}
     * @throws IOException when {@link java.io.File} constructor throws
     * IOException.
     */
    public static void readFromFile(String file, SudokuSolver solver) throws IOException {
        FileInputStream f = new FileInputStream(new File(file));
        Scanner in = new Scanner(f);
        solver.getBoard().clear();
        while (in.hasNextLine()) {
            CLISet.getInstance().perform(in.nextLine().split(" "), solver);
        }
        in.close();
    }
}
