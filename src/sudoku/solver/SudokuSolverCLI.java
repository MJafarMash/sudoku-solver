package sudoku.solver;

import java.util.HashMap;
import java.util.Scanner;
import sudoku.solver.CLI.Action;
import sudoku.solver.CLI.CLIClear;
import sudoku.solver.CLI.CLIHelp;
import sudoku.solver.CLI.CLILoad;
import sudoku.solver.CLI.CLIQuit;
import sudoku.solver.CLI.CLISet;
import sudoku.solver.CLI.CLIShowBoard;
import sudoku.solver.CLI.CLISolve;

/**
 *
 * @author mjafar
 */
public class SudokuSolverCLI {
    private static boolean finished;
    public static final HashMap<String, Action> actions = new HashMap<>();

    static {
        actions.put("show", CLIShowBoard.getInstance());
        actions.put("set", CLISet.getInstance());
        actions.put("help", CLIHelp.getInstance());
        actions.put("quit", CLIQuit.getInstance());
        actions.put("exit", CLIQuit.getInstance());
        actions.put("load", CLILoad.getInstance());
        actions.put("solve", CLISolve.getInstance());
        actions.put("clear", CLIClear.getInstance());

        finished = false;
    }

    /**
     * Provide a simple shell for user to interact with program.
     *
     * @param solver instance of {@link #SudokuSolver}
     */
    protected static void getData(SudokuSolver solver) {
        System.out.println("+------------------------------+");
        System.out.println("|                              |");
        System.out.println("|         Sudoku Solver        |");
        System.out.println("|                              |");
        System.out.println("+------------------------------+");
        System.out.println();


        Scanner in = new Scanner(System.in);

        String command;
        String[] parts;
        String[] args;

        while (!finished) {
            System.out.print("$ ");
            command = in.nextLine();
            command = command.trim();
            if (command.length() == 0) {
                continue;
            }
            parts = command.split(" ", 2);
            if (parts.length == 2) {
                args = parts[1].split(" ");
            } else {
                args = new String[]{};
            }
            if (!actions.containsKey(parts[0])) {
                System.err.printf("Command `%s' not found.\n", parts[0]);
                CLIHelp.getInstance().perform(args, solver);
                continue;
            }
            actions.get(parts[0]).perform(args, solver);
        }


        in.close();
    }

    /**
     * Closes the program
     */
    public static void kill() {
        finished = true;
    }

}
