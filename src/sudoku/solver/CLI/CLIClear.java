package sudoku.solver.CLI;

import sudoku.solver.SudokuSolver;

/**
 * Reset the table.
 *
 * @author mjafar
 */
public class CLIClear extends Action {

    private static CLIClear instance;

    public static CLIClear getInstance() {
        if (instance == null) {
            instance = new CLIClear();
        }
        return instance;
    }

    @Override
    public void perform(String[] args, SudokuSolver solver) {
        solver.getBoard().clear();
    }
}
