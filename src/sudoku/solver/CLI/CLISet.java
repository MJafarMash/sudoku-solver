package sudoku.solver.CLI;

import sudoku.solver.SudokuSolver;

/**
 *
 * @author mjafar
 */

public final class CLISet extends Action {

    private static CLISet instance;

    public static CLISet getInstance() {
        if (instance == null) {
            instance = new CLISet();
        }
        return instance;
    }

    @Override
    public void perform(String[] args, SudokuSolver solver) {
        int row;
        int col;
        int number;
        try {
            row = Integer.parseInt(args[0]);
            col = Integer.parseInt(args[1]);
            number = Integer.parseInt(args[2]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Wrong number of arguments.\nUsage:\n\tSet rowNumber columnNumber initialValue");
            return;
        }

        try {
            solver.setInitialValue(row, col, number);
        } catch (IllegalArgumentException e) {
            System.err.println("Wrong input!\n" + e.getMessage() + "\nUsage:\n\tSet rowNumber columnNumber initialValue");
        }

    }
}