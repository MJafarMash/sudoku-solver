package sudoku.solver.CLI;

import sudoku.solver.SudokuSolver;
import sudoku.solver.SudokuSolverCLI;

/**
 *
 * @author mjafar
 */
public final class CLIHelp extends Action {

    private static CLIHelp instance;

    public static CLIHelp getInstance() {
        if (instance == null) {
            instance = new CLIHelp();
        }
        return instance;
    }

    @Override
    public void perform(String[] args, SudokuSolver solver) {
        System.out.println();
        System.out.println("List of supported commands:");
        int n = 0;
        for (String c : SudokuSolverCLI.actions.keySet()) {
            System.out.printf("%s\t", c);
            if (n++ == 2) {
                n = 0;
                System.out.println();
            }
        }
        System.out.println();
    }
}