package sudoku.solver.CLI;

import sudoku.solver.SudokuSolver;

/**
 *
 * @author mjafar
 */
public abstract class Action {
    public abstract void perform(String[] args, SudokuSolver solver);
}
