package sudoku.solver.CLI;

import sudoku.solver.SudokuSolver;
import sudoku.solver.SudokuSolverCLI;

/**
 *
 * @author mjafar
 */
public final class CLIQuit extends Action {

    private static CLIQuit instance;

    public static CLIQuit getInstance() {
        if (instance == null) {
            instance = new CLIQuit();
        }
        return instance;
    }

    @Override
    public void perform(String[] args, SudokuSolver solver) {
        SudokuSolverCLI.kill();
    }
}