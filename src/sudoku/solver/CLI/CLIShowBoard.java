/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku.solver.CLI;

import sudoku.solver.SudokuSolver;

/**
 *
 * @author mjafar
 */
public final class CLIShowBoard extends Action {

    private static CLIShowBoard instance;

    public static CLIShowBoard getInstance() {
        if (instance == null) {
            instance = new CLIShowBoard();
        }
        return instance;
    }

    @Override
    public void perform(String[] args, SudokuSolver solver) {
        System.out.println();
        final int cols = 9 + // numbers
                //9 + // space before
                3 + // space after last in 3 group
                3 + 1; // separators
        final int rows = 3 + 1 + // separators
                3 * 3; // lines

        final char topRight = (char) 0x2557;
        final char topLeft = (char) 0x2554;
        final char botRight = (char) 0x255D;
        final char botLeft = (char) 0x255A;
        final char horizontal = (char) 0x2550;
        final char vertical = (char) 0x2551;
        final char rightOnlyIntersect = (char) 0x2560;
        final char leftOnlyIntersect = (char) 0x2563;
        final char bottomOnlyIntersect = (char) 0x2566;
        final char topOnlyIntersect = (char) 0x2569;
        final char intersect = (char) 0x256C;

        int r = 0;
        int c = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (i % 4 == 0) {
                    // separator rows
                    if (i == 0 && j == 0) {
                        System.out.print(topLeft);
                    } else if (i == 0 && j == cols - 1) {
                        System.out.print(topRight);
                    } else if (i == 0 && j % 5 == 0) {
                        System.out.print(bottomOnlyIntersect);
                    } else if (i == rows - 1 && j == 0) {
                        System.out.print(botLeft);
                    } else if (i == rows - 1 && j == cols - 1) {
                        System.out.print(botRight);
                    } else if (i == rows - 1 && j % 5 == 0) {
                        System.out.print(topOnlyIntersect);
                    } else {
                        if (j == 0) {
                            System.out.print(rightOnlyIntersect);
                        } else if (j == cols - 1) {
                            System.out.print(leftOnlyIntersect);
                        } else if (j % 5 == 0) {
                            System.out.print(intersect);
                        } else {
                            System.out.print(horizontal);
                            if ((j + 1) % 5 != 0) {
                                System.out.print(horizontal);
                            }
                        }
                    }
                } else {
                    if (j % 5 == 0) {
                        System.out.print(vertical);
                    } else if ((j + 1) % 5 == 0) {
                        System.out.print(" ");
                    } else {
                        if (args.length == 0) {
                            System.out.printf(" %s",
                                    solver.getBoard().get(r + 1, c + 1).isSet()
                                    ? String.valueOf(solver.getBoard().get(r + 1, c + 1).getValue())
                                    : " ");
                        } else {
                            System.out.printf(" %s",
                                    solver.getBoard().get(r + 1, c + 1).isSet()
                                    ? "-"
                                    : String.valueOf(solver.getBoard().get(r + 1, c + 1).getPossibleValues().size()));
                        }
                        c++;
                    }
                }
            }
            System.out.println();
            if (i % 4 != 0) {
                r++;
            }
            c = 0;
        }
    }
}
