/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku.solver.CLI;

import java.io.IOException;
import sudoku.solver.SudokuSolver;
import sudoku.solver.SudokuSolverRunner;

/**
 *
 * @author mjafar
 */
public class CLILoad extends Action {

    private static CLILoad instance;

    public static CLILoad getInstance() {
        if (instance == null) {
            instance = new CLILoad();
        }
        return instance;
    }

    @Override
    public void perform(String[] args, SudokuSolver solver) {
        StringBuilder address;
        address = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            address.append(args[i]);
            address.append(' ');
        }
        address.trimToSize();
        address.deleteCharAt(address.lastIndexOf(" "));
        try {
            SudokuSolverRunner.readFromFile(address.toString(), solver);
        } catch (IOException e) {
            System.err.println("Error reading from " + address.toString() + "\n" + e.getMessage());
        }
    }

}
