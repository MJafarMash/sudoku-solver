package sudoku.solver.CLI;

import sudoku.solver.SolutionFoundException;
import sudoku.solver.SudokuSolver;
import sudoku.solver.UnsolvablePuzzleException;

/**
 *
 * @author mjafar
 */
public class CLISolve extends Action {

    private static CLISolve instance;

    public static CLISolve getInstance() {
        if (instance == null) {
            instance = new CLISolve();
        }
        return instance;
    }

    @Override
    public void perform(String[] args, SudokuSolver solver) {
        try {
            solver.solve();
        } catch (SolutionFoundException e) {
            CLIShowBoard.getInstance().perform(args, solver);
        } catch (UnsolvablePuzzleException e) {
            System.out.println("This sudoku is unsolvable. There are some cells that cannot accept any values.\n"
                    + "Use `show probalbe' command for more information.");
        }
    }
}
