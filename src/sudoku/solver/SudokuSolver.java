package sudoku.solver;

import java.util.TreeSet;
import sudoku.solver.CLI.CLIShowBoard;

/**
 * Solve Sudoku Puzzles.
 *
 * @author mjafar
 */
public final class SudokuSolver {

    private SudokuBoard board;
    public boolean showBackTrackingTree = false;

    /**
     * Initializes class
     */
    public SudokuSolver() {
        board = new SudokuBoard();
    }

    /**
     * Get board of numbers
     *
     * @return {@link #SudokuBoard}
     */
    public SudokuBoard getBoard() {
        return board;
    }

    /**
     * Set initial value of a cell
     *
     * @param row row
     * @param col column
     * @param value number to be set
     * @throws IllegalArgumentException when row or col are out of board or
     * value is not acceptable
     */
    public void setInitialValue(int row, int col, int value) throws IllegalArgumentException {
        if (value < 1 || value > 9) {
            throw new IllegalArgumentException("Value should be between 1 and 9");
        }

        if (board.repetetiveInBlock(row, col, value)) {
            throw new IllegalArgumentException("Value is repetetive in it's respective 3x3 block.");
        }

        if (board.repetetiveInRow(row, value)) {
            throw new IllegalArgumentException("Value is repetetive in it's respective row.");
        }

        if (board.repetetiveInColumn(col, value)) {
            throw new IllegalArgumentException("Value is repetetive in it's respective column.");
        }

        board.get(row, col).setValue(value);
        board.removeValueFromPossibilitiesInRowColBox(row, col, value);
    }

    /**
     * Solves the sudoku
     */
    public void solve() throws UnsolvablePuzzleException, SolutionFoundException {
        int numberOfNonZeros;
        int newNumberOfNonZeros;

        do {
            findPossibleAnswers();
            numberOfNonZeros = board.getNumberOfNonZeros();
            while (board.hasOnlyOnePossibeNumberAnswers()) {
                board.setOnlyOnePossibleNumberAswers();
            }
            setRegionalAnswers();
            newNumberOfNonZeros = board.getNumberOfNonZeros();
        } while (newNumberOfNonZeros - numberOfNonZeros > 0); // While we can update board with trivial answers

        if (board.hasZeros()) {
            throw new UnsolvablePuzzleException();
        }

        backTrack();
    }
    private int Depth = 0;

    /**
     * Use backtracking algorithm to solve the puzzle
     *
     */
    private void backTrack() throws SolutionFoundException, UnsolvablePuzzleException {
        if (board.getNumberOfNonZeros() == 81) {
            throw new SolutionFoundException();
        }

        int minRow = 0;
        int minCol = 0;
        int minValue = 10;
        findPossibleAnswers();
        for (int row = 1; row <= 9; row++) {
            for (int col = 1; col <= 9; col++) {
                SudokuBoardCell currentCell = board.get(row, col);
                if (currentCell.isSet()) {
                    continue;
                }
                if (!currentCell.hasPossibleValues()) {
                    throw new UnsolvablePuzzleException();
                }
                if (minValue == 2) {
                    break;
                }
                if (currentCell.possibleValuesCount() == 2 || currentCell.possibleValuesCount() < minValue) {
                    minRow = row;
                    minCol = col;
                    minValue = currentCell.possibleValuesCount();
                }
            }
        }

        Depth++;

        for (int possibleValue = 1; possibleValue <= 9; possibleValue++) {
            if (board.repetetiveInBlock(minRow, minCol, possibleValue)
                    || board.repetetiveInColumn(minCol, possibleValue)
                    || board.repetetiveInRow(minRow, possibleValue)) {
                continue;
            }
            board.get(minRow, minCol).setValue(possibleValue);

            try {
                backTrack();
            } catch (UnsolvablePuzzleException e) {
                if (showBackTrackingTree) {
                    p();
                    System.err.println("(" + minRow + ", " + minCol + ") := " + possibleValue + " is incorrect." + board.get(minRow, minCol).getPossibleValues());
                }
            } catch (SolutionFoundException e) {
                if (showBackTrackingTree) {
                    System.err.println("  " + possibleValue + " is correct.");
                }
                Depth--;
                throw e;
            }
        }
        board.get(minRow, minCol).setValue(0);
        findPossibleAnswers();
        Depth--;
    }

    /**
     * Prints Depth * 2 spaces
     */
    private void p() {
        for (int i = 1; i < Depth; i++) {
            System.err.print("  ");
        }
    }

    /**
     * Set answers based on rows, columns, boxes that have 8 of 9 answers
     * revealed
     */
    private void setRegionalAnswers() {
        int lastPossibleAnswer;
        int numberOfPossibleAnswersInThisRegion;

        for (int row = 1; row <= 9; row++) {
            for (int col = 1; col <= 9; col++) {
                if (board.get(row, col).isSet()) {
                    continue;
                }

                // For row
                lastPossibleAnswer = 0;
                numberOfPossibleAnswersInThisRegion = 0;
                for (int checkNo = 1; checkNo <= 9; checkNo++) {
                    if (!board.repetetiveInRow(row, checkNo)) {
                        lastPossibleAnswer = checkNo;
                        numberOfPossibleAnswersInThisRegion++;
                        if (numberOfPossibleAnswersInThisRegion > 1) {
                            break;
                        }
                    }
                }
                if (numberOfPossibleAnswersInThisRegion == 1) {
                    if (!board.repetetiveInColumn(col, lastPossibleAnswer)
                            && !board.repetetiveInBlock(row, col, lastPossibleAnswer)) {
                        board.get(row, col).setValue(lastPossibleAnswer);
                        board.removeValueFromPossibilitiesInRowColBox(row, col, lastPossibleAnswer);
                        continue;
                    }
                }

                // For column
                lastPossibleAnswer = 0;
                numberOfPossibleAnswersInThisRegion = 0;
                for (int checkNo = 1; checkNo <= 9; checkNo++) {
                    if (!board.repetetiveInColumn(col, checkNo)) {
                        lastPossibleAnswer = checkNo;
                        numberOfPossibleAnswersInThisRegion++;
                        if (numberOfPossibleAnswersInThisRegion > 1) {
                            break;
                        }
                    }
                }
                if (numberOfPossibleAnswersInThisRegion == 1) {
                    if (!board.repetetiveInRow(row, lastPossibleAnswer)
                            && !board.repetetiveInBlock(row, col, lastPossibleAnswer)) {
                        board.get(row, col).setValue(lastPossibleAnswer);
                        board.removeValueFromPossibilitiesInRowColBox(row, col, lastPossibleAnswer);
                        continue;
                    }
                }

                // For box
                lastPossibleAnswer = 0;
                numberOfPossibleAnswersInThisRegion = 0;
                for (int checkNo = 1; checkNo <= 9; checkNo++) {
                    if (!board.repetetiveInBlock(row, col, checkNo)) {
                        lastPossibleAnswer = checkNo;
                        numberOfPossibleAnswersInThisRegion++;
                        if (numberOfPossibleAnswersInThisRegion > 1) {
                            break;
                        }
                    }
                }
                if (numberOfPossibleAnswersInThisRegion == 1) {
                    if (!board.repetetiveInColumn(col, lastPossibleAnswer)
                            && !board.repetetiveInRow(row, lastPossibleAnswer)) {
                        board.get(row, col).setValue(lastPossibleAnswer);
                        board.removeValueFromPossibilitiesInRowColBox(row, col, lastPossibleAnswer);
                        continue;
                    }
                }
            }
        }
    }

    /**
     * Find possible answers based on unique-ness in row and column and box
     */
    private void findPossibleAnswers() {
        for (int row = 1; row <= 9; row++) {
            for (int col = 1; col <= 9; col++) {
                if (board.get(row, col).isSet()) {
                    continue;
                }
                for (int checkNo = 1; checkNo <= 9; checkNo++) {
                    if (!board.repetetiveInRow(row, checkNo)
                            && !board.repetetiveInColumn(col, checkNo)
                            && !board.repetetiveInBlock(row, col, checkNo)) {
                        board.get(row, col).addPossibleValue(checkNo);
                    } else {
                        board.get(row, col).removePossibleValue(checkNo);
                    }
                }
            }
        }
    }
}
