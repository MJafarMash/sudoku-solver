package sudoku.solver;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author mjafar
 */
public final class SudokuBoardCell {
    private Integer value;
    private TreeSet<Integer> possibleValues;

    /**
     * Constructs an empty cell
     */
    public SudokuBoardCell() {
        value = 0;
        possibleValues = new TreeSet<>();
    }

    /**
     * Get <i>int</i> value of cell
     * @return
     */
    public int getValue() {
        return value.intValue();
    }

    /**
     * If cell values is set
     * @return true if a number is assigned to cell
     */
    public boolean isSet() {
        return value > 0;
    }

    /**
     * If there is possible values for cell
     * @return true if <i>possibleValues</i> is not empty
     */
    public boolean hasPossibleValues() {
        return possibleValues.size() > 0;
    }

    /**
     * Removes possibility of having value <i>v</i> if it's a possibility.
     * @param v
     */
    public void removePossibleValue(int v) {
        possibleValues.remove(v);
    }

    /**
     * Adds value <code>v</code> to possible values <b>Set</b>
     * @param v
     */
    public void addPossibleValue(int v) {
        possibleValues.add(v);
    }

    /**
     * Adds all values in <code>v</code> to possible values <b>Set</b>
     * @param v
     */
    public void addPossibleValues(TreeSet<Integer> v) {
        possibleValues.addAll(v);
    }

    /**
     * Returns number of possible values
     * @return
     */
    public int possibleValuesCount() {
        return possibleValues.size();
    }

    /**
     * Returns possible values set
     *
     * @return
     */
    public TreeSet<Integer> getPossibleValues() {
        return possibleValues;
    }



    /**
     * Set value of cell to <i>v</i> and removes all possible values.
     * @param v
     */
    public void setValue(int v) {
        value = Integer.valueOf(v);
    }
}
