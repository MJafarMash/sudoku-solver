/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku.solver.swing;

import java.io.File;
import java.io.IOException;
import sudoku.solver.SolutionFoundException;
import sudoku.solver.UnsolvablePuzzleException;

/**
 *
 * @author mjafar
 */
public class SudokuSolverGUI extends javax.swing.JFrame {
    protected sudoku.solver.SudokuSolver solver;
    private SudokuCellGUI[][] fileds;

    /**
     * Creates new form SudokuSolverGUI
     */
    public SudokuSolverGUI() {
        initComponents();
    }

    /**
     * Creates new form SudokuSolverGUI
     */
    public SudokuSolverGUI(sudoku.solver.SudokuSolver solver) {
        this.solver = solver;
        initComponents();
        fileds = new SudokuCellGUI[9][9];
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 9; j++) {
                fileds[i - 1][j - 1] = new SudokuCellGUI(i, j, this);
                pnlTable.add(fileds[i - 1][j - 1]);
            }
        }
    }


    /**
     * Update the text boxes
     */
    private void update() {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 9; j++) {
                fileds[i - 1][j - 1].setText(solver.getBoard().get(i, j).isSet() ? String.valueOf(solver.getBoard().get(i, j).getValue()) : "");
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTable = new javax.swing.JPanel();
        btnLoad = new javax.swing.JButton();
        btnSolve = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sudoku Solver");
        setBounds(new java.awt.Rectangle(200, 200, 0, 0));
        setResizable(false);

        pnlTable.setBorder(null);
        pnlTable.setLayout(new java.awt.GridLayout(9, 9, 3, 3));

        btnLoad.setText("Load");
        btnLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadActionPerformed(evt);
            }
        });

        btnSolve.setText("Solve");
        btnSolve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSolveActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnSave.setText("Save");
        btnSave.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTable, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnLoad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSolve)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClear)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlTable, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLoad)
                    .addComponent(btnSolve)
                    .addComponent(btnClear)
                    .addComponent(btnSave))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSolveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSolveActionPerformed
        try {
            solver.solve();
        } catch (UnsolvablePuzzleException ex) {
            javax.swing.JOptionPane.showMessageDialog(this,
                    "This puzzle is unsolvable, there are some cells that cannot accept any values.",
                    "No Solution Found",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        } catch (SolutionFoundException ex) {
        } finally {
            update();
        }
    }//GEN-LAST:event_btnSolveActionPerformed

    private void btnLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadActionPerformed
        javax.swing.JFileChooser fileChooser;
        fileChooser = new javax.swing.JFileChooser(new File("./"));
        fileChooser.showOpenDialog(this);
        fileChooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
        fileChooser.setVisible(true);
        try {
            String path;
            path = fileChooser.getSelectedFile().getAbsolutePath();
            sudoku.solver.SudokuSolverRunner.readFromFile(path, solver);
        } catch (IOException ex) {
            javax.swing.JOptionPane.showMessageDialog(this,
                    "There's a problem in reading the selected file.\n" + ex.getMessage(),
                    "IO problem",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        } catch (NullPointerException ex) {
            // Cancelled
        } finally {
            update();
        }
    }//GEN-LAST:event_btnLoadActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        solver.getBoard().clear();
        update();
    }//GEN-LAST:event_btnClearActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnLoad;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSolve;
    private javax.swing.JPanel pnlTable;
    // End of variables declaration//GEN-END:variables
}
