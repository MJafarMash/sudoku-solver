package sudoku.solver.swing;

/**
 * A class that extends {@link JTextFiled}
 */
public class SudokuCellGUI extends javax.swing.JTextField {
    private static java.awt.Font font;
    static {
        font = new java.awt.Font("Arial Black", java.awt.Font.PLAIN, 18);
    }
    public SudokuCellGUI(final int i, final int j, final SudokuSolverGUI s) {
        setInputVerifier(new javax.swing.InputVerifier() {
            @Override
            public boolean verify(javax.swing.JComponent input) {
                String txt = ((SudokuCellGUI) input).getText().trim();
                if (txt.length() == 0) {
                    return true;
                }
                if (txt.length() > 1) {
                    return false;
                }
                if ("0".equals(txt)) {
                    ((SudokuCellGUI) input).setText("");
                    return true;
                }
                if (Integer.valueOf(txt) < 1 || Integer.valueOf(txt) > 9) {
                    return false;
                }
                return true;
            }
        });
        addFocusListener(new java.awt.event.FocusListener() {
            @Override
            public void focusGained(java.awt.event.FocusEvent e) {
            }

            @Override
            public void focusLost(java.awt.event.FocusEvent e) {
                String txt = getText().trim();
                if (txt.length() == 0 || "0".equals(txt)) {
                    s.solver.getBoard().get(i, j).setValue(0);
                } else {
                    s.solver.getBoard().get(i, j).setValue(Integer.valueOf(txt));
                }
            }
        });

        setFont(font);
        setHorizontalAlignment(javax.swing.JTextField.CENTER);
    }

}
